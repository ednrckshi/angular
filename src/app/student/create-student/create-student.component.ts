import { Observable } from 'rxjs';
import { MajorService } from './../../major/major.service';
import { StudentService } from './../student.service';
import { Component, OnInit } from '@angular/core';
import { Student } from '../student';
import { ActivatedRoute, Router } from '@angular/router';
import { Major } from 'src/app/major/major';

@Component({
  selector: 'app-create-student',
  templateUrl: './create-student.component.html',
  styleUrls: ['./create-student.component.scss']
})
export class CreateStudentComponent implements OnInit {
  student: Student = new Student();
  id: number;
  submitted = false;
  majors: Observable<Major[]>;

  constructor(private route: ActivatedRoute, private studentService: StudentService, private router: Router, private majorService: MajorService) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];
    
    if(this.id != null){
      this.studentService.getStudent(this.id).subscribe(
        data => {
          console.log(data);
          this.student = data;
        },
        error => console.log(error)
      );
    }

    //load major
    this.majors = this.majorService.getMajorList();
    console.log(this.majors)
  }

  newStudent():void{
    this.submitted = false;
    this.student = new Student();
  }

  save(){
    this.studentService.createStudent(this.student).subscribe(
      data => {
        console.log(data);
        this.student = new Student();
        this.gotoList();
      },
      error => console.log(error)
    );
  }

  updateStudent(){
    this.studentService.updateStudent(this.id, this.student).subscribe(
      data => {
        console.log(data);
        this.student = new Student();
        this.gotoList();
      },
      error => console.log(error)
    )
  }

  onSubmit(){
    if(this.student.year >= 1990 && this.student.year <= 2020){
      if(this.id!=null){
        this.updateStudent();
        console.log("put");
      }
      else{
        this.save();
        console.log("post");
      }
      this.submitted = true;
    }
    
    
  }

  gotoList(){
    this.router.navigate(['/students']);
  }

  log(x){
    console.log(x);
  }

}
