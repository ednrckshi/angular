import { StudentService } from './../student.service';
import { Student } from './../student';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-student-detail',
  templateUrl: './student-detail.component.html',
  styleUrls: ['./student-detail.component.scss']
})
export class StudentDetailComponent implements OnInit {
  id: number;
  student: Student;

  constructor(private route:ActivatedRoute, private router: Router, private studentService: StudentService) { }

  ngOnInit(): void {
    this.student = new Student();
    this.id = this.route.snapshot.params['id'];

    this.studentService.getStudent(this.id).subscribe(
      data => {
        console.log(data);
        this.student = data;
      },
      error => console.log(error)
    )
  }

  list(){
    this.router.navigate(['students']);
  }


}
