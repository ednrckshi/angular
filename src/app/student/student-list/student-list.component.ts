import { StudentService } from './../student.service';
import { Student } from './../student';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-student-list',
  templateUrl: './student-list.component.html',
  styleUrls: ['./student-list.component.scss']
})
export class StudentListComponent implements OnInit {
  students: Observable<Student[]>;

  constructor(private studentService: StudentService, private router: Router) { }

  ngOnInit(): void {
    this.reloadData();
  }

  reloadData(){
    this.students = this.studentService.getStudentList();
    console.log(this.students);
  }

  deleteStudent(event:any, student:Student){
    event.stopPropagation();
    if(confirm("Are you sure?")){
      this.studentService.deleteStudent(student.id, student).subscribe(
        data => {
          console.log(data);
          this.reloadData();
        },
        error => console.log(error)
      );
    }
  }

  updateStudent(event:any, id:number){
    event.stopPropagation();
    this.router.navigate(['update',id]);
  }

  studentDetail(id:number){
    this.router.navigate(['detail',id]);
  }

}
