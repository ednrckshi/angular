export class Student{
    id: number;
    fname: string;
    lname: string;
    email: string;
    year: number;
    address: string;
    major: number;
    active: boolean;
}