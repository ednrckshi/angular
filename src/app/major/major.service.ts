import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MajorService {
  private baseUrl = 'http://localhost:8080/majors';

  constructor(private http: HttpClient) { }

  getMajorList(): Observable<any>{
    return this.http.get(`${this.baseUrl}`);
  }
}
