export class Major{
    id: number;
    major: string;
    active: boolean;
}